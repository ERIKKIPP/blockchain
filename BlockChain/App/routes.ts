﻿import { Routes } from "@angular/router"
import { HashComponent } from "./Components/hash.component";
import { BlockComponent } from "./Components/block.component";
import { HomeComponent } from "./Components/home.component";
import { BlockchainComponent } from "./Components/blockchain.component";
import { DistributedComponent } from "./Components/distributed.component";
import { TokensComponent } from "./Components/tokens.component";
import { CoinbaseComponent } from "./Components/coinbase.component";

export const appRoutes: Routes = [
    { path: "", component: HomeComponent, pathMatch: "full" },
    { path: "hash", component: HashComponent },
    { path: "block", component: BlockComponent },
    { path: "blockchain", component: BlockchainComponent },
    { path: "distributed", component: DistributedComponent },
    { path: "tokens", component: TokensComponent },
    { path: "coinbase", component: CoinbaseComponent }
    
];