﻿import { Component } from "@angular/core";

@Component({
    templateUrl: "./block.component.html",
    styleUrls: ["./block.component.css"],
    moduleId: module.id
})
export class BlockComponent {

    constructor() {
        console.log("BlockComponent -> constructor");
    }
}
