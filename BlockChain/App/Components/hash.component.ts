﻿import { Component } from "@angular/core";
import { ToastrService } from "../Services/toastr.service";
import { CryptoService } from "../Services/crypto.service";

@Component({
    templateUrl: "./hash.component.html",
    styleUrls: ["./hash.component.css"],
    moduleId: module.id
})
export class HashComponent {
    constructor(private toastr: ToastrService, private crypto: CryptoService) {
        console.log("HashComponent -> constructor");
    }

    sha256(value) {
        // calculate a SHA256 hash of the contents of the block
        return this.crypto.sha256(value);
    }

}
