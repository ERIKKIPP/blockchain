import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { BrowserModule } from "@angular/platform-browser";
import { AppComponent } from "./Components/app.component";
import { HashComponent } from "./Components/hash.component";
import { BlockComponent } from "./Components/block.component";
import { DashboardComponent } from "./Components/dashboard.component";
import { BlockchainComponent } from "./Components/blockchain.component";
import { DistributedComponent } from "./Components/distributed.component";
import { TokensComponent } from "./Components/tokens.component";
import { CoinbaseComponent } from "./Components/coinbase.component";
import { HomeComponent } from "./Components/home.component";
import { ToastrService } from "./Services/toastr.service";
import { CryptoService } from "./Services/crypto.service";
import { appRoutes} from "./routes";

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot(appRoutes)
    ],
  declarations:
  [
      AppComponent,
      HashComponent,
      BlockComponent,
      DashboardComponent,
      HomeComponent,
      BlockchainComponent,
      DistributedComponent,
      TokensComponent,
      CoinbaseComponent
  ],
  providers: [ToastrService, CryptoService],
  bootstrap:[AppComponent]
})
export class AppModule {}